CREATE TABLE IF NOT EXISTS server_tag(
  tag                               TEXT                         NOT NULL,
  username                          TEXT                         NOT NULL,
  published                         TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  edited                            TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  fingerprint                       TEXT                         NOT NULL,
  status                            TEXT                         NOT NULL,
PRIMARY KEY(tag, fingerprint)
);

CREATE TABLE IF NOT EXISTS server_note(
  note_id                           BIGINT GENERATED ALWAYS AS IDENTITY,
  note                              TEXT                         NOT NULL,
  username                          TEXT                         NOT NULL,
  published                         TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  edited                            TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  fingerprint                       TEXT                         NOT NULL,
  status                            TEXT                         NOT NULL,
PRIMARY KEY(note_id, fingerprint)
);
