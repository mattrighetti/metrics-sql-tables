CREATE TABLE IF NOT EXISTS server_log (
  published                           TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  physical_host                       TEXT,
  virtual_host                        TEXT,
  digest                              TEXT,
  PRIMARY KEY(digest)
);

CREATE TABLE IF NOT EXISTS log_line (
  published                           TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  log_digest                          TEXT references server_log(digest),
  ip                                  TEXT,
  method                              TEXT,
  protocol                            TEXT,
  request                             TEXT,
  size                                BIGINT,
  response                            INTEGER,
  is_valid                            BOOLEAN,
  digest                              TEXT,
  PRIMARY KEY(digest)
);
