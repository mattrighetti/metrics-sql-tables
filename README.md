# Metrics SQL Tables

This project contains metrics SQL tables that are used to store Tor network
documents.

These are used by several projects across the Tor metrics ecosystem.
