CREATE TABLE IF NOT EXISTS server_descriptor(
is_bridge                           BOOLEAN                      NOT NULL,
published                           TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
nickname                            TEXT                         NOT NULL,
fingerprint                         TEXT                         NOT NULL,
digest_sha1_hex                     TEXT                         NOT NULL,
identity_ed25519                    TEXT,
master_key_ed25519                  TEXT,
address                             TEXT,
or_port                             TEXT,
socks_port                          TEXT,
dir_port                            TEXT,
or_addresses                        TEXT,
bandwidth_rate                      BIGINT,
bandwidth_burst                     BIGINT,
bandwidth_observed                  BIGINT,
platform                            TEXT,
overload_general_timestamp          BIGINT,
overload_general_version            INTEGER,
protocols                           TEXT,
is_hibernating                      BOOLEAN,
uptime                              BIGINT,
onion_key                           TEXT,
signing_key                         TEXT,
exit_policy                         TEXT,
contact                             TEXT,
bridge_distribution_request         TEXT,
family                              TEXT,
read_history                        TEXT,
write_history                       TEXT,
uses_enhanced_dns_logic             BOOLEAN,
caches_extra_info                   BOOLEAN,
extra_info_sha1_digest              TEXT,
extra_info_sha256_digest            TEXT,
is_hidden_service_dir               BOOLEAN,
link_protocol_version               INTEGER[],
circuit_protocol_version            INTEGER[],
allow_single_hop_exits              BOOLEAN,
ipv6_default_policy                 TEXT,
ipv6_port_list                      TEXT,
ntor_onion_key                      TEXT,
onion_key_cross_cert                TEXT,
ntor_onion_key_cross_cert           TEXT,
ntor_onion_key_cross_cert_sign      INTEGER,
tunnelled_dir_server                BOOLEAN,
router_sig_ed25519                  TEXT,
router_signature                    TEXT,
header                              TEXT,
PRIMARY KEY(digest_sha1_hex)
);

CREATE index ON server_descriptor(published);
CREATE INDEX server_fingerprint_published ON server_descriptor (fingerprint, published DESC);

CREATE TABLE IF NOT EXISTS microdescriptor(
  digest                            TEXT                         NOT NULL,
  time                              TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  or_addresses                      TEXT                         NOT NULL,
  policy                            TEXT,
  port_list                         TEXT,
  onion_key                         TEXT,
  ntor_onion_key                    TEXT,
  family_entries                    TEXT,
  ipv6_policy                       TEXT,
  ipv6_port_list                    TEXT,
  rsa_identity                      TEXT,
  ed25519_identity                  TEXT,
  PRIMARY KEY(digest));

CREATE index ON microdescriptor(time);
