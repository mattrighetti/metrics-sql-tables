CREATE TABLE IF NOT EXISTS server_status(
  is_bridge                           BOOLEAN                      NOT NULL,
  published                           TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  nickname                            TEXT                         NOT NULL,
  fingerprint                         TEXT                         NOT NULL,
  or_addresses                        TEXT,
  last_seen                           TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  first_seen                          TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  running                             BOOLEAN,
  flags                               TEXT,
  country                             TEXT,
  country_name                        TEXT,
  autonomous_system                   TEXT,
  as_name                             TEXT,
  verified_host_names                 TEXT,
  last_restarted                      TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  exit_policy                         TEXT,
  contacts                            TEXT,
  platform                            TEXT,
  version                             TEXT,
  version_status                      TEXT,
  effective_family                    TEXT,
  declared_family                     TEXT,
  transport                           TEXT,
  bridgedb_distributor                TEXT,
  blocklist                           TEXT,
  last_changed_address_or_port        TIMESTAMP WITHOUT TIME ZONE,
  diff_or_addresses                   TEXT,
  unverified_host_names               TEXT,
  unreachable_or_addresses            TEXT,
  overload_ratelimits_version         INTEGER,
  overload_ratelimits_timestamp       TIMESTAMP WITHOUT TIME ZONE,
  overload_ratelimits_ratelimit       BIGINT,
  overload_ratelimits_burstlimit      BIGINT,
  overload_ratelimits_read_count      BIGINT,
  overload_ratelimits_write_count     BIGINT,
  overload_fd_exhausted_version       INTEGER,
  overload_fd_exhausted_timestamp     TIMESTAMP WITHOUT TIME ZONE,
  overload_general_timestamp          TIMESTAMP WITHOUT TIME ZONE,
  overload_general_version            INTEGER,
  server_descriptor_digest            TEXT,
  first_network_status_digest         TEXT,
  family_digest                       TEXT,
  PRIMARY KEY(fingerprint, nickname, published)
);

CREATE index ON server_status(published);
CREATE index ON server_status(fingerprint);
CREATE index ON server_status(nickname);

CREATE INDEX server_status_optimized_index
ON server_status (fingerprint, nickname, published DESC);

CREATE TABLE IF NOT EXISTS server_families(
  digest                              TEXT,
  effective_family                    TEXT,
  members                             INTEGER,
  published                           TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  updated                             TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  verified                            BOOLEAN,
  PRIMARY KEY(digest)
);

CREATE EXTENSION IF NOT EXISTS pg_trgm;
CREATE INDEX server_families_effective_family_trgm_idx ON server_families USING gin (effective_family gin_trgm_ops);
